import { User } from './entities/user.entity'
import { UserLoginHistory } from './entities/userLoginHistory.entity'
import { RefreshToken } from './entities/refreshToken.entity'
import { UserWallet } from './entities/userWallet.entity'
import { Transaction } from './entities/transaction.entity'

import { createConnection, getConnection, ConnectionOptions } from 'typeorm'

export const mysql_config : ConnectionOptions = {
    type: "mysql",
    host: 'us-cdbr-east-03.cleardb.com',
    port: 3306,
    username: 'bc4c3bce367ad2',
    password: '86f4f379',
    database: 'heroku_bbe604a93c2e2cf',
    entities: [
        User,
        UserLoginHistory,
        RefreshToken,
        UserWallet,
        Transaction
    ],
    synchronize: true,
    logging: false,
    connectTimeout: 30000,
    acquireTimeout: 30000
}

export const connection = {
    async create(){
      await createConnection(mysql_config);
    },
  
    async close(){
      await getConnection().close(); 
    },
}

export const dummyUserId = 144