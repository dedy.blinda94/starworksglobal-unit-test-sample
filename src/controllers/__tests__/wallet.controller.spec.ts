import createServer from '../../server'
import { connection } from '../../connectionDummy'

const faker = require('faker') //https://github.com/Marak/faker.js
const supertest = require('supertest')
const app = createServer() // new

beforeAll(async () => {
  await connection.create()

  await app.listen(5000, () => {
    console.log("Server has started!")
  })
})

afterAll(async ()=>{
    await connection.close()
})

/** ================= POST /api/wallet/balance ================= */
describe("POST /api/wallet/balance", () => {
  describe("when No token provided", () => {
    it("Should respond with a 401 status code", async () => {
      const body = {
        
      }
      
      const res = await supertest(app)
                          .post('/api/wallet/balance')
                          .set('Accept', 'application/json')
                          .set('x-access-token', '')
                          .send(body)
      expect(res.statusCode).toBe(401)
    })
  })

  describe("when userId and token valid", () => {
    it("Should respond with a 200 status code", async () => {
      const body = {
        userId : "24"
      }
      
      const res = await supertest(app)
                          .post('/api/wallet/balance')
                          .set('Accept', 'application/json')
                          .set('x-access-token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQsImlhdCI6MTYzMDIxMzc0MH0.MlJHz_7phLa94KhJg25nTAn0O-CNSjSlhfhVn3-uw4M')
                          .send(body)
      expect(res.statusCode).toBe(200)
    })
  })

  describe("when Failed to authenticate token.", () => {
    it("Should respond with a 406 status code", async () => {
      const body = {
        userId : "24"
      }
      
      const res = await supertest(app)
                          .post('/api/wallet/balance')
                          .set('Accept', 'application/json')
                          .set('x-access-token', 'faketoken')
                          .send(body)
      expect(res.statusCode).toBe(406)
    })
  })
})

/** ================= POST /api/wallet/topup ================= */
describe("POST /api/wallet/topup", () => {
  describe("when No token provided", () => {
    it("Should respond with a 401 status code", async () => {
      const body = {
        
      }
      
      const res = await supertest(app)
                          .post('/api/wallet/topup')
                          .set('Accept', 'application/json')
                          .set('x-access-token', '')
                          .send(body)
      expect(res.statusCode).toBe(401)
    })
  })

  describe("when userId, amount and token valid", () => {
    it("Should respond with a 200 status code", async () => {
      const body = {
        userId : "24",
        amount : "1000000"
      }
      
      const res = await supertest(app)
                          .post('/api/wallet/balance')
                          .set('Accept', 'application/json')
                          .set('x-access-token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQsImlhdCI6MTYzMDIxMzc0MH0.MlJHz_7phLa94KhJg25nTAn0O-CNSjSlhfhVn3-uw4M')
                          .send(body)
      expect(res.statusCode).toBe(200)
    })
  })

  describe("when Failed to authenticate token.", () => {
    it("Should respond with a 406 status code", async () => {
      const body = {
        userId : "24"
      }
      
      const res = await supertest(app)
                          .post('/api/wallet/topup')
                          .set('Accept', 'application/json')
                          .set('x-access-token', 'faketoken')
                          .send(body)
      expect(res.statusCode).toBe(406)
    })
  })
})

/** ================= POST /api/wallet/payment ================= */
describe("POST /api/wallet/payment", () => {
  describe("when No token provided", () => {
    it("Should respond with a 401 status code", async () => {
      const body = {
        
      }
      
      const res = await supertest(app)
                          .post('/api/wallet/payment')
                          .set('Accept', 'application/json')
                          .set('x-access-token', '')
                          .send(body)
      expect(res.statusCode).toBe(401)
    })
  })

  describe("when success process transaction and payment", () => {
    it("Should respond with a 200 status code", async () => {
      const body = {
        userId : "24",
        merchantId : "1",
        amount : "50000"
      }
      
      const res = await supertest(app)
                          .post('/api/wallet/payment')
                          .set('Accept', 'application/json')
                          .set('x-access-token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQsImlhdCI6MTYzMDIxMzc0MH0.MlJHz_7phLa94KhJg25nTAn0O-CNSjSlhfhVn3-uw4M')
                          .send(body)
      expect(res.statusCode).toBe(200)
    })
  })

  describe("when Failed to authenticate token.", () => {
    it("Should respond with a 406 status code", async () => {
      const body = {
        userId : "24"
      }
      
      const res = await supertest(app)
                          .post('/api/wallet/payment')
                          .set('Accept', 'application/json')
                          .set('x-access-token', 'faketoken')
                          .send(body)
      expect(res.statusCode).toBe(406)
    })
  })
})