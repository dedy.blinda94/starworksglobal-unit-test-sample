import createServer from '../../server'
import { connection } from '../../connectionDummy'

const faker = require('faker') //https://github.com/Marak/faker.js
const supertest = require('supertest')
const app = createServer() // new

beforeAll(async () => {
  await connection.create()

  await app.listen(5000, () => {
    console.log("Server has started!")
  })
})

afterAll(async ()=>{
    await connection.close()
})

/** ================= POST /api/users/login ================= */
describe("POST /api/users/login", () => {
  describe("when the username or password is missing", () => {
    it("Should respond with a 400 status code", async () => {
      const body = {
        
      }
      
      const res = await supertest(app)
                          .post('/api/users/login')
                          .set('Accept', 'application/json')
                          .set('user-agent', 'PostmanRuntime/7.26.8')
                          .send(body)
      expect(res.statusCode).toBe(400)
    })
  })
  
  describe("when passed a username and password", () => {
    it("Should respond with a 200 status code", async () => {
      const body = {
        emailAddress: 'dedy@gmail.com',
        password: '123',
      }
      
      const res = await supertest(app)
                          .post('/api/users/login')
                          .set('Accept', 'application/json')
                          .set('user-agent', 'PostmanRuntime/7.26.8')
                          .send(body)
      expect(res.statusCode).toBe(200)
    })
  })

  describe("when passed a username and password and it's not correct", () => {
    it("Should respond with a 401 status code", async () => {
      const body = {
        emailAddress: 'dedyy@gmail.com',
        password: '12345',
      }
      
      const res = await supertest(app)
                          .post('/api/users/login')
                          .set('Accept', 'application/json')
                          .set('user-agent', 'PostmanRuntime/7.26.8')
                          .send(body)
      expect(res.statusCode).toBe(401)
    })
  })
})

/** ================= POST /api/users/token ================= */
describe("POST /api/users/token", () => {
  describe("when the data not complete", () => {
    it("Should respond with a 400 status code", async () => {
      const body = {
        
      }
      
      const res = await supertest(app)
                          .post('/api/users/token')
                          .set('Accept', 'application/json')
                          .send(body)
      expect(res.statusCode).toBe(400)
    })
  })
  
  describe("when the data correct and succeed get token", () => {
    it("Should respond with a 200 status code", async () => {
      const body = {
        userId: '24',
        emailAddress: 'dedy@gmail.com',
      }
      
      const res = await supertest(app)
                          .post('/api/users/token')
                          .set('Accept', 'application/json')
                          .send(body)
      expect(res.statusCode).toBe(200)
    })
  })

  describe("when the data correct and not successful get token", () => {
    it("Should respond with a 401 status code", async () => {
      const body = {
        userId: '64',
        emailAddress: 'ab@gmail.com',
      }
      
      const res = await supertest(app)
                          .post('/api/users/token')
                          .set('Accept', 'application/json')
                          .send(body)
      expect(res.statusCode).toBe(401)
    })
  })

  describe("when the data user not found", () => {
    it("Should respond with a 404 status code", async () => {
      const body = {
        userId: '545',
        emailAddress: 'dedy25@gmail.com',
      }
      
      const res = await supertest(app)
                          .post('/api/users/token')
                          .set('Accept', 'application/json')
                          .send(body)
      expect(res.statusCode).toBe(404)
    })
  })
})

/** ================= POST /api/users/registration ================= */
describe("POST /api/users/registration", () => {
  describe("when the data not complete", () => {
    it("Should respond with a 400 status code", async () => {
      const body = {
        
      }
      
      const res = await supertest(app)
                          .post('/api/users/registration')
                          .set('Accept', 'application/json')
                          .send(body)
      expect(res.statusCode).toBe(400)
    })
  })
  
  describe("when the data email already in use", () => {
    it("Should respond with a 400 status code", async () => {
      const body = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        dateOfBirth: "2000-01-01",
        streetAddress: faker.address.streetName(),
        city: faker.address.city(),
        province: faker.address.county(),
        telephoneNumber: faker.phone.phoneNumber(),
        emailAddress: "dedy@gmail.com",
        password:"123"
      }
      
      const res = await supertest(app)
                          .post('/api/users/registration')
                          .set('Accept', 'application/json')
                          .send(body)
      expect(res.statusCode).toBe(400)
    })
  })

  describe("when the data correct and data saving", () => {
    it("Should respond with a 201 status code", async () => {
      const body = {
        firstName:"Dedy",
        lastName:"Blinda",
        dateOfBirth:"1994-09-06",
        streetAddress:"Perumahan Taman Penta 2",
        city:"Badung",
        province:"Bali",
        telephoneNumber:"6285857676414",
        emailAddress: faker.internet.email(),
        password:"123"
      }
      const res = await supertest(app)
                          .post('/api/users/registration')
                          .set('Accept', 'application/json')
                          .send(body)
      expect(res.statusCode).toBe(201)    
    })
  })

  describe("when the data error", () => {
    it("Should respond with a 400 status code", async () => {
      const body = {
        userId: '545',
        emailAddress: 'dedy25@gmail.com',
      }
      
      const res = await supertest(app)
                          .post('/api/users/registration')
                          .set('Accept', 'application/json')
                          .send(body)
      expect(res.statusCode).toBe(400)
    })
  })
})