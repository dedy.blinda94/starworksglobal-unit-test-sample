import walletService from '../wallet.services'

import { connection, dummyUserId } from '../../connectionDummy'

describe('walletService', () => {
    beforeEach(async () => {
        await connection.create()
    })

    afterEach(async ()=>{
        await connection.close()
    })

    describe('createWallet', () => {
        it('Should complete add new wallet', async () => {
            const response = await walletService.createWallet(dummyUserId)
            expect(response).not.toBeNull()
        })
    })

    describe('topUp', () => {
        it('If userId correct sum amount', async () => {
            const response = await walletService.topUp(dummyUserId, 100000)
            expect(response).not.toBeNull()
        })

        it('If userId not correct return null', async () => {
            const response = await walletService.topUp(dummyUserId+55, 100000)
            expect(response).toBeNull()
        })
    })
    
    describe('createTransaction', () => {
        it('Should success add new transaction', async () => {
            const response = await walletService.createTransaction(
                dummyUserId,
                100000,
                1,
                1
            )
            expect(response).not.toBeNull()
        })
    })

    describe('payment', () => {
        it('If userId correct decrease amount', async () => {
            const response = await walletService.payment(dummyUserId, 100000)
            expect(response).not.toBeNull()
        })

        it('If userId not correct return null', async () => {
            const response = await walletService.payment(dummyUserId+55, 100000)
            expect(response).toBeNull()
        })
    })
})