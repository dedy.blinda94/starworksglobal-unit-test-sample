import userService from '../user.service'

import { connection, dummyUserId } from '../../connectionDummy'

describe('userService', () => {
    beforeEach(async () => {
        await connection.create()
    })

    afterEach(async ()=>{
        await connection.close()
    })

    describe('registration', () => {
        it('Should complete add new user', async () => {
            var faker = require('faker') //https://github.com/Marak/faker.js
            const response = await userService.register(
                faker.name.firstName(),
                faker.name.lastName(),
                new Date(),
                faker.address.streetName(),
                faker.address.city(),
                faker.address.county(),
                faker.phone.phoneNumber(),
                faker.internet.email(),
                '123',
                new Date(),
            )
            expect(response).not.toBeNull()
        })
    })

    describe('login', () => {
        it('If user registered and password true', async () => {
            const response = await userService.login(
                'dedy@gmail.com',
                '123',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0'
            )
            expect(response).not.toBeNull()
        })

        it('If user registered and worng password return null', async () => {
            const response = await userService.login(
                'dedy@gmail.com',
                '1234',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0'
            )
            expect(response).toBeNull()
        })
    })

    describe('createLoginHistory', () => {
        it('Should complete saving login history', async () => {
            const response = await userService.createLoginHistory(
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0',
                24,
                1
            )
            expect(response).not.toBeNull()
        })
    })

    describe('generateToken', () => {
        it('Should complete generate and insert token', async () => {
            const response = await userService.generateToken(dummyUserId)
            expect(response).not.toBeNull()
        })
    })
})