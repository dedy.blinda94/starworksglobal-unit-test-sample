import express from 'express';

import usersRoutes from './routes/users.routes';
import walletRoutes from './routes/wallet.routes';

const bodyParser = require('body-parser')

function createServer() {
	const app = express()
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({extended: true}));
	app.use("/api/users", usersRoutes)
	app.use('/api/wallet', walletRoutes)
	return app
}

export default createServer;